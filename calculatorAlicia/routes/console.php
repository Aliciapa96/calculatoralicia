<?php

use Illuminate\Foundation\Inspiring;


Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('hello2', function () {
    $this->info('This command is by console');
})->describe('Our own comand');
