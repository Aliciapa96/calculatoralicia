<?php

namespace Tests\Feature;
use App\Http\Controllers\calculatorController;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OperationTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    public function testAdd()
    {
        $calculator=new calculatorController(1,4);
        $calculator->add();
        $this->assertEquals(5, $calculator->resultvalue);
    }
    public function testsubtract()
    {
        $calculator=new calculatorController(1,4);
        $calculator->subtract();
        $this->assertEquals(-3, $calculator->resultvalue);
    }
    public function testMultiplication()
    {
        $calculator=new calculatorController(2,9);
        $calculator->multiplication();
        $this->assertEquals(18, $calculator->resultvalue);
    }
    public function testDivision()
    {
        $calculator=new calculatorController(4,2);
        $calculator->division();
        $this->assertEquals(2, $calculator->resultvalue);
    }
    
}
