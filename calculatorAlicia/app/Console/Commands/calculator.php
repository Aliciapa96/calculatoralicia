<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\calculatorController;
use App\Http\Controllers\Aplication\UseCase\Calculator\Calculator_Operation_Controll;

class calculator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculator';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This is make command command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
          /*Asking to the user the numbers*/;
          $first_number=$this->ask('Introduce the first number');
          $second_number=$this->ask('Introduce the second number');
          /**Instance The calculatorController*/

          $operations= new calculatorController($first_number,$second_number);
          $operations->add();
          $operations->calculated();
          $operations->subtract();
          $operations->calculated();
          $operations->multiplication();
          $operations->calculated();
          $operations->division();     
          $operations->calculated();
          $operations->second_grade_equation();
          $operations->calculated();
    }
}
