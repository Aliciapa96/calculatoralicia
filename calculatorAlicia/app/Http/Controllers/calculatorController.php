<?php

namespace App\Http\Controllers;
use App\calculator;
use Illuminate\Http\Request;

class calculatorController extends Controller
{
    public $resultvalue, $operation , $first_number, $second_number;
    public function __construct($first_number, $second_number)
    {
        $this->first_number=$first_number;
        $this->second_number=$second_number;
    }
    /* Calculate the add*/
    public function add()
    {
        $this->resultvalue=$this->first_number+$this->second_number;
        $this->operation="+";
     print_r("The operation result for add is ".$this->resultvalue."\n");
    }
    /*Calculate the substract*/
    public function subtract()
    {
        $this->operation="-";
        $this->resultvalue=$this->first_number-$this->second_number;
     print_r("The operation result for subtract is ". $this->resultvalue."\n");
    }
    /*Calculate the multiplicacion*/
    public function multiplication()
    {
        $this->operation="*";
        $this->resultvalue=$this->first_number*$this->second_number;
     print_r("The operation result for multiplication is ". $this->resultvalue."\n");
    }
    /*Calculate the division*/
    public function division()
    {
        $this->operation="/";
        $this->resultvalue=$this->first_number/$this->second_number;
     print_r("The operation result for division is ".$this->resultvalue."\n");
    }
    /*Calculate the second grade equation*/
    public function second_grade_equation()
    {

        $c_value=5;
        $this->operation="2nd";
        $operation1 = pow($this->second_number,2); 
        $operation2 = 4*$this->first_number*$c_value; 
        $subs = $operation1-$operation2; 
        $root = sqrt ($subs); 
        $two = 2*$this->first_number; 
        $b = $this->second_number * -1; 

       $equationres1 = ($b + $root)/$two;
       $equationres2 = ($b - $root)/$two;

        
    print_r("The operation result for second grade equation are ".$equationres1." and equation ".$equationres2."\n");
        $this->resultvalue=$equationres1." and ".$equationres2;
    }
    /*Adding the method to database*/
    public function calculated()
    {
        $request = new \Illuminate\Http\Request();
        $request->replace(['first_number' => $this->first_number, 'second_number'=>$this->second_number]);

        $this->validate($request, [
            'first_number' => 'required|numeric',
            'second_number' => 'required|numeric'
        ]);
        
        $operation= new calculator();
        $operation->first_number= $this->first_number;
        $operation->operation= $this->operation;
        $operation->second_number= $this->second_number;
        $operation->result= $this->resultvalue;
        $operation->created_result_operation= date("Y-m-d H:i:s");
        // add other fields
        $operation->save();
    }
}
