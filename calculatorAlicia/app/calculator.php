<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class calculator extends Model
{
    protected $table = 'calculator';
    protected $fillable = [
        'id', 'first_number','operation','second_number','result','created_result_operation'
    ];
}
